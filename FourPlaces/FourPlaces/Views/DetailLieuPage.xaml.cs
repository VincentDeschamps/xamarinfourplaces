﻿using FourPlaces.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TD.Api.Dtos;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FourPlaces.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DetailLieuPage : ContentPage
    {
        public DetailLieuPage(PlaceItemSummary placeItem)
        {
            InitializeComponent();
            BindingContext = new DetailLieuViewModel(placeItem);
        }
    }
}