﻿using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace FourPlaces.ViewModels
{
    class DetailLieuViewModel : ViewModelBase
    {

        private string _title;
        private string _description;
        private double _latitude;
        private double _longitude;
        private int _id;
        private int _imageId;
        public PlaceItemSummary PlaceItem;
        public ICommand GoToProfileCommand { get; }
        public ICommand GoToMapCommand { get; }
        public ICommand GoToCommentsCommand { get; }

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        public double Latitude
        {
            get => _latitude;
            set => SetProperty(ref _latitude, value);
        }

        public double Longitude
        {
            get => _longitude;
            set => SetProperty(ref _longitude, value);
        }

        public int Id
        {
            get => _id;
            set => SetProperty(ref _id, value);
        }

        public int ImageId
        {
            get => _imageId;
            set => SetProperty(ref _imageId, value);
        }

        public DetailLieuViewModel(PlaceItemSummary placeItem)
        {
            Id = placeItem.Id;
            Title = placeItem.Title;
            Description = placeItem.Description;
            Latitude = placeItem.Latitude;
            Longitude = placeItem.Longitude;
            ImageId = placeItem.ImageId;
            GoToProfileCommand = new Command(GoToProfile);
            GoToMapCommand = new Command(GoToMap);
            GoToCommentsCommand = new Command(GoToComments);
            PlaceItem = placeItem;
        }

        public async void GoToProfile()
        {
            await App.NavigationService.NavigateAsync("ProfilePage");
        }

        public async void GoToMap()
        {
            await App.NavigationService.NavigateAsync("MapPage", PlaceItem);
        }
        public async void GoToComments()
        {
            await App.NavigationService.NavigateAsync("CommentsPage", PlaceItem.Id);
        }
    }
}
