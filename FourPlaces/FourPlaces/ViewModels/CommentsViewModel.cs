﻿using Common.Api.Dtos;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ViewModels
{
    class CommentsViewModel : ViewModelBase
    {
        private List<CommentItem> _comments;
        private string _messageText;
        public ICommand GoToProfileCommand { get; }
        public ICommand SendCommentCommand { get; }
        public int PlaceId;

        public List<CommentItem> Comments
        {
            get => _comments;
            set => SetProperty(ref _comments, value);
        }

        public string MessageText
        {
            get => _messageText;
            set => SetProperty(ref _messageText, value);
        }

        public CommentsViewModel(int placeId)
        {
            PlaceId = placeId;
            GoToProfileCommand = new Command(GoToProfile);
            SendCommentCommand = new Command(SendComment);
            GetComments();
        }

        public async void GetComments()
        {
            ApiClient apiClient = new ApiClient();
            HttpResponseMessage response = await apiClient.Execute(HttpMethod.Get, "https://td-api.julienmialon.com/places/"+PlaceId);
            Response<PlaceItem> result = await apiClient.ReadFromResponse<Response<PlaceItem>>(response);
            if (result.IsSuccess)
            {
                Comments = result.Data.Comments;
            }
        }

        public async void SendComment()
        {
            if (MessageText != null)
            {
                CreateCommentRequest createCommentRequest = new CreateCommentRequest();
                createCommentRequest.Text = MessageText;

                ApiClient apiClient = new ApiClient();
                HttpResponseMessage response = await apiClient.Execute(HttpMethod.Post, "https://td-api.julienmialon.com/places/" + PlaceId + "/comments", createCommentRequest, App._token);
                Response result = await apiClient.ReadFromResponse<Response>(response);

                if (result.IsSuccess)
                {
                    MessageText = "";

                    HttpResponseMessage response2 = await apiClient.Execute(HttpMethod.Get, "https://td-api.julienmialon.com/places/" + PlaceId);
                    Response<PlaceItem> result2 = await apiClient.ReadFromResponse<Response<PlaceItem>>(response2);
                    if (result2.IsSuccess)
                    {
                        Comments = result2.Data.Comments;
                    }
                }
            }
        }

        public async void GoToProfile()
        {
            await App.NavigationService.NavigateAsync("ProfilePage");
        }
    }
}
