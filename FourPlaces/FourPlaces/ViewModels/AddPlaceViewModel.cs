﻿using Acr.UserDialogs;
using Common.Api.Dtos;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Essentials;
using Xamarin.Forms;

namespace FourPlaces.ViewModels
{
    class AddPlaceViewModel : ViewModelBase
    {
        private ImageSource _imageSource;
        byte[] imageData;
        private string _title;
        private string _description;
        private string _latitude;
        private string _longitude;
        private bool _warningVisible;
        private string _warningText;
        private string _warningColor;
        private bool _warningVisibleCoord;
        private string _warningTextCoord;
        private string _warningColorCoord;

        public ICommand TakeImageFromGalleryCommand { get; }
        public ICommand TakePhotoCommand { get; }
        public ICommand AddPlaceCommand { get; }
        public ICommand GetCoordCommand { get; }
        public ICommand GoToProfileCommand { get; }

        public ImageSource ImageSource
        {
            get => _imageSource;
            set => SetProperty(ref _imageSource, value);
        }

        public string Title
        {
            get => _title;
            set => SetProperty(ref _title, value);
        }

        public string Description
        {
            get => _description;
            set => SetProperty(ref _description, value);
        }

        public string Latitude
        {
            get => _latitude;
            set => SetProperty(ref _latitude, value);
        }

        public string Longitude
        {
            get => _longitude;
            set => SetProperty(ref _longitude, value);
        }

        public bool WarningVisible
        {
            get => _warningVisible;
            set => SetProperty(ref _warningVisible, value);
        }

        public string WarningText
        {
            get => _warningText;
            set => SetProperty(ref _warningText, value);
        }

        public string WarningColor
        {
            get => _warningColor;
            set => SetProperty(ref _warningColor, value);
        }

        public bool WarningVisibleCoord
        {
            get => _warningVisibleCoord;
            set => SetProperty(ref _warningVisibleCoord, value);
        }

        public string WarningTextCoord
        {
            get => _warningTextCoord;
            set => SetProperty(ref _warningTextCoord, value);
        }

        public string WarningColorCoord
        {
            get => _warningColorCoord;
            set => SetProperty(ref _warningColorCoord, value);
        }

        public AddPlaceViewModel()
        {
            TakeImageFromGalleryCommand = new Command(SelectImageFromGallery);
            TakePhotoCommand = new Command(OpenCamera);
            AddPlaceCommand = new Command(Validate);
            GetCoordCommand = new Command(GetCoord);
            GoToProfileCommand = new Command(GoToProfile);
            WarningVisible = false;
            WarningVisibleCoord = false;
        }

        public async void SelectImageFromGallery()
        {
            WarningVisible = false;
            WarningColor = "Default";
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                Console.WriteLine("Photos Not Supported");
                return;
            }

            try
            {
                Stream stream = null;
                /*var file = await CrossMedia.Current.PickPhotoAsync();*/

                var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                {
                    PhotoSize = PhotoSize.Small
                });

                if (file == null)
                    return;

                ImageSource = ImageSource.FromStream(() =>
                {
                    stream = file.GetStream();
                    return stream;
                });

                using (var memoryStream = new MemoryStream())
                {
                    file.GetStream().CopyTo(memoryStream);
                    file.Dispose();
                    imageData = memoryStream.ToArray();
                }

            }
            catch (Exception){}
        }

        public async void OpenCamera()
        {
            WarningVisible = false;
            WarningColor = "Default";

            if (!CrossMedia.Current.IsCameraAvailable || !CrossMedia.Current.IsTakePhotoSupported)
            {
                return;
            }
            try
            {
                var file = await CrossMedia.Current.TakePhotoAsync(new StoreCameraMediaOptions
                {
                    PhotoSize = PhotoSize.Small
                });

                if (file == null)
                    return;

                ImageSource = ImageSource.FromStream(() =>
                {
                    var stream = file.GetStream();
                    return stream;
                });

                using (var memoryStream = new MemoryStream())
                {
                    file.GetStream().CopyTo(memoryStream);
                    file.Dispose();
                    imageData = memoryStream.ToArray();
                }
            }
            catch (Exception) { }
        }

        public async Task<int> AddImage(ByteArrayContent imageContent)
        {
            ApiClient apiClient = new ApiClient();
            HttpResponseMessage response = await apiClient.Execute(HttpMethod.Post, "https://td-api.julienmialon.com/images", imageContent);
            Response<ImageItem> result = await apiClient.ReadFromResponse<Response<ImageItem>>(response);

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Image Ajoutée");
                return result.Data.Id;
            }
            else
            {
                Console.WriteLine("Erreur");
                return -1;
            }
        }

        public async void AddPlace(int imageId)
        {
            ApiClient apiClient = new ApiClient();

            CreatePlaceRequest placeRequest = new CreatePlaceRequest();
            placeRequest.Title = Title;
            placeRequest.Description = Description;
            placeRequest.ImageId = imageId;
            placeRequest.Latitude = Double.Parse(Latitude);
            placeRequest.Longitude = Double.Parse(Longitude);

            HttpResponseMessage response = await apiClient.Execute(HttpMethod.Post, "https://td-api.julienmialon.com/places", placeRequest, App._token);
            Response result = await apiClient.ReadFromResponse<Response>(response);

            if (response.IsSuccessStatusCode)
            {
                WarningText = "Le lieu a été ajouté";
                WarningVisible = true;
                WarningColor = "Green";
            }
            else {
                WarningText = "Il y a eu une erreur";
                WarningVisible = true;
                WarningColor = "Red";
            }
        }

        public async void GetCoord()
        {
            WarningColorCoord = "Blue";
            WarningVisibleCoord = true;
            WarningTextCoord = "Récupération des coordonnées...";
            try
            {
                var location = await Geolocation.GetLocationAsync();

                if (location != null)
                {
                    WarningVisibleCoord = false;
                    Latitude = location.Latitude + "";
                    Longitude = location.Longitude + "";
                }
            }
            catch (Exception)
            {
                WarningColorCoord = "Red";
                WarningVisibleCoord = true;
                WarningTextCoord = "Impossible de récupérer les coordonnées, le gps est-il activé ?";
            }   
        }

        public async void Validate()
        {
            WarningColor = "Blue";
            WarningText = "Requête en cours...";
            WarningVisible = true;
            if (ImageSource==null || Title==null || Description==null || Latitude==null || Longitude==null)
            {
                WarningColor = "Blue";
                WarningText = "Veuillez remplir tous les champs et selectionner une image";
                WarningVisible = true;
                return;
            }

            var imageContent = new ByteArrayContent(imageData);
            imageContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");

            int imageId = await AddImage(imageContent);
            if (imageId == -1)
            {
                Console.WriteLine("Erreur imageId=-1");
                return;
            }
            else
            {
                AddPlace(imageId);
            }
        }

        public async void GoToProfile()
        {
            await App.NavigationService.NavigateAsync("ProfilePage");
        }
    }
}
