﻿using Common.Api.Dtos;
using Plugin.Media;
using Plugin.Media.Abstractions;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ViewModels
{
    class ProfileViewModel : ViewModelBase
    {
        private string _email;
        private string _lastName;
        private string _firstName;
        private string _warningColor;
        private string _warningText;
        private bool _warningVisible;
        private ImageSource _imageSource;
        private string _oldPassword;
        private string _newPassword;


        public byte[] imageData;
        public ICommand ValidateCommand { get; }
        public ICommand AddImageCommand { get; }
        public ICommand ChangePasswordCommand { get; }

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }
        public string LastName
        {
            get => _lastName;
            set => SetProperty(ref _lastName, value);
        }
        public string FirstName
        {
            get => _firstName;
            set => SetProperty(ref _firstName, value);
        }
        public string WarningColor
        {
            get => _warningColor;
            set => SetProperty(ref _warningColor, value);
        }
        public string WarningText
        {
            get => _warningText;
            set => SetProperty(ref _warningText, value);
        }
        public bool WarningVisible
        {
            get => _warningVisible;
            set => SetProperty(ref _warningVisible, value);
        }
        public ImageSource ImageSource
        {
            get => _imageSource;
            set => SetProperty(ref _imageSource, value);
        }
        public string OldPassword
        {
            get => _oldPassword;
            set => SetProperty(ref _oldPassword, value);
        }
        public string NewPassword
        {
            get => _newPassword;
            set => SetProperty(ref _newPassword, value);
        }

        public ProfileViewModel()
        {
            OldPassword = "";
            NewPassword = "";
            AddImageCommand = new Command(SelectImageFromGallery);
            ValidateCommand = new Command(Validate);
            ChangePasswordCommand = new Command(ChangePassword);
            GetProfile();
        }

        public async void GetProfile()
        {
            ApiClient apiClient = new ApiClient();
            HttpResponseMessage response = await apiClient.Execute(HttpMethod.Get, "https://td-api.julienmialon.com/me", null, App._token);
            Response<UserItem> result = await apiClient.ReadFromResponse<Response<UserItem>>(response);

            if (response.IsSuccessStatusCode)
            {
                Email = result.Data.Email;
                FirstName = result.Data.FirstName;
                LastName = result.Data.LastName;
                ImageSource = result.Data.ImageId == null ? "" : "https://td-api.julienmialon.com/images/" + (result.Data.ImageId).ToString();
            }
        }

        public async void SelectImageFromGallery()
        {
            WarningVisible = false;
            WarningColor = "Default";
            if (!CrossMedia.Current.IsPickPhotoSupported)
            {
                Console.WriteLine("Photos Not Supported");
                return;
            }

            try
            {
                Stream stream = null;
                /*var file = await CrossMedia.Current.PickPhotoAsync();*/

                var file = await CrossMedia.Current.PickPhotoAsync(new PickMediaOptions
                {
                    PhotoSize = PhotoSize.Small
                });

                if (file == null)
                    return;

                ImageSource = ImageSource.FromStream(() =>
                {
                    stream = file.GetStream();
                    return stream;
                });

                using (var memoryStream = new MemoryStream())
                {
                    file.GetStream().CopyTo(memoryStream);
                    file.Dispose();
                    imageData = memoryStream.ToArray();
                }

            }
            catch (Exception) { }
        }


        public async Task<int?> AddImage(ByteArrayContent imageContent)
        {
            ApiClient apiClient = new ApiClient();
            HttpResponseMessage response = await apiClient.Execute(HttpMethod.Post, "https://td-api.julienmialon.com/images", imageContent);
            Response<ImageItem> result = await apiClient.ReadFromResponse<Response<ImageItem>>(response);

            if (response.IsSuccessStatusCode)
            {
                Console.WriteLine("Image Ajoutée");
                return result.Data.Id;
            }
            else
            {
                Console.WriteLine("Erreur");
                return null;
            }
        }

        public async void Validate()
        {
            WarningColor = "Blue";
            WarningText = "Requête en cours...";
            WarningVisible = true;
            if (LastName == "" || FirstName == "")
            {
                WarningColor = "Blue";
                WarningText = "Veuillez remplir votre prénom et vote nom";
                WarningVisible = true;
                return;
            }
            else
            {
                int? imageId;
                if (imageData != null)
                {
                    var imageContent = new ByteArrayContent(imageData);
                    imageContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/jpeg");
                    imageId = await AddImage(imageContent);
                }
                else
                {
                    imageId = null;
                }

                UpdateProfileRequest updateProfileRequest = new UpdateProfileRequest();
                updateProfileRequest.FirstName = FirstName;
                updateProfileRequest.LastName = LastName;
                updateProfileRequest.ImageId = imageId;

                ApiClient apiClient = new ApiClient();
                HttpResponseMessage response = await apiClient.Execute(new HttpMethod("PATCH"), "https://td-api.julienmialon.com/me", updateProfileRequest, App._token);
                Response<ImageItem> result = await apiClient.ReadFromResponse<Response<ImageItem>>(response);

                if (response.IsSuccessStatusCode)
                {
                    WarningText = "Le profil a été modifié";
                    WarningVisible = true;
                    WarningColor = "Green";
                }
                else
                {
                    WarningText = "Il y a eu une erreur";
                    WarningVisible = true;
                    WarningColor = "Red";
                }
            }
        }

        public async void ChangePassword()
        {
            WarningColor = "Blue";
            WarningText = "Requête en cours...";
            WarningVisible = true;

            if (OldPassword == "" || NewPassword == "")
            {
                WarningColor = "Blue";
                WarningText = "Veuillez remplir votre ancien mot de passe et en choisir un nouveau";
                WarningVisible = true;
                return;
            }
            else
            {
                UpdatePasswordRequest updatePasswordRequest = new UpdatePasswordRequest();
                updatePasswordRequest.OldPassword = OldPassword;
                updatePasswordRequest.NewPassword = NewPassword;

                ApiClient apiClient = new ApiClient();
                HttpResponseMessage response = await apiClient.Execute(new HttpMethod("PATCH"), "https://td-api.julienmialon.com/me/password", updatePasswordRequest, App._token);
                Response result = await apiClient.ReadFromResponse<Response>(response);

                if (response.IsSuccessStatusCode)
                {
                    WarningText = "Le profil a été modifié";
                    WarningVisible = true;
                    WarningColor = "Green";
                }
                else if (result.ErrorMessage == "Invalid password")
                {
                    WarningText = "Mot de passe invalide";
                    WarningColor = "Red";
                    WarningVisible = true;
                }
                else
                {
                    WarningText = "Il y a eu une erreur";
                    WarningVisible = true;
                    WarningColor = "Red";
                }
            }
        }
    }
}
