﻿using Common.Api.Dtos;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ViewModels
{
    class ListeLieuxViewModel : ViewModelBase
    {
        private List<PlaceItemSummary> _lieux;

        private PlaceItemSummary _selectedPlaceItem;
        public PlaceItemSummary SelectedPlaceItem
        {
            get => _selectedPlaceItem;
            set
            {
                if (SetProperty(ref _selectedPlaceItem, value) && value != null)
                {
                    SelectedPlaceItem = null;
                    GoToDetail(value);
                }
            }
        }

        public ICommand GoToAddPlaceCommand { get; }
        public ICommand GoToProfileCommand { get; }

        public List<PlaceItemSummary> Lieux
        {
            get => _lieux;
            set => SetProperty(ref _lieux, value);
        }

        public ListeLieuxViewModel()
        {
            GetLieux();
            GoToAddPlaceCommand = new Command(GoToAddPlace);
            GoToProfileCommand = new Command(GoToProfile);
        }

        public async void GetLieux()
        {
            ApiClient apiClient = new ApiClient();
            HttpResponseMessage response = await apiClient.Execute(HttpMethod.Get, "https://td-api.julienmialon.com/places");
            Response<List<PlaceItemSummary>> result = await apiClient.ReadFromResponse<Response<List<PlaceItemSummary>>>(response);
            if (result.IsSuccess)
            {
                Lieux = result.Data;
            }
        }

        public async void GoToAddPlace()
        {
            await App.NavigationService.NavigateAsync("AddPlacePage");
        }

        public async void GoToDetail(PlaceItemSummary item)
        {
            await App.NavigationService.NavigateAsync("DetailLieuPage", item);
        }

        public async void GoToProfile()
        {
            await App.NavigationService.NavigateAsync("ProfilePage");
        }
    }
}
