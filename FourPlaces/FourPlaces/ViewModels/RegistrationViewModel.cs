﻿using Common.Api.Dtos;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ViewModels
{
    class RegistrationViewModel : ViewModelBase
    {
        private string _warningText;
        private string _warningColor;
        private bool _warningVisible;
        private string _email;
        private string _firstName;
        private string _lastName;
        private string _password;

        public string WarningText
        {
            get => _warningText;
            set => SetProperty(ref _warningText, value);
        }

        public string WarningColor
        {
            get => _warningColor;
            set => SetProperty(ref _warningColor, value);
        }

        public bool WarningVisible
        {
            get => _warningVisible;
            set => SetProperty(ref _warningVisible, value);
        }

        public string Email
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }
        public string FirstName
        {
            get => _firstName;
            set => SetProperty(ref _firstName, value);
        }
        public string LastName
        {
            get => _lastName;
            set => SetProperty(ref _lastName, value);
        }
        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }
        public ICommand GoToConnexionCommand { get; }
        public ICommand PostRegistrationCommand { get; }

        public RegistrationViewModel()
        {
            GoToConnexionCommand = new Command(GoToConnexion);
            PostRegistrationCommand = new Command(PostRegistration);
        }

        public async void PostRegistration()
        {
            WarningText = "Inscription en cours...";
            WarningColor = "Blue";
            WarningVisible = true;
            RegisterRequest registerRequest = new RegisterRequest();
            registerRequest.Email = Email;
            registerRequest.FirstName = FirstName;
            registerRequest.LastName = LastName;
            registerRequest.Password = Password;

            ApiClient apiClient = new ApiClient();

            HttpResponseMessage response = await apiClient.Execute(HttpMethod.Post, "https://td-api.julienmialon.com/auth/register", registerRequest);
            Response<LoginResult> result = await apiClient.ReadFromResponse<Response<LoginResult>>(response);

            if (response.IsSuccessStatusCode)
            {
                App._token = result.Data.AccessToken;
                await App.NavigationService.NavigateAsync("ListeLieuxPage");
                WarningVisible = false;
            }
            else
            {
                WarningText = "Inscription impossible";
                WarningColor = "Red";
                WarningVisible = true;
            }
        }

        public async void GoToConnexion()
        {
            await App.NavigationService.NavigateAsync("ConnexionPage");
        }
    }
}
