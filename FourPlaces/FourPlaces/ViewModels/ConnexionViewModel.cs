﻿using Common.Api.Dtos;
using Storm.Mvvm;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;

namespace FourPlaces.ViewModels
{
    class ConnexionViewModel : ViewModelBase
    {
        private string _email;
        private string _password;
        private bool _warningVisible;
        private string _warningText;
        private string _warningColor;

        public string Email 
        {
            get => _email;
            set => SetProperty(ref _email, value);
        }

        public string Password
        {
            get => _password;
            set => SetProperty(ref _password, value);
        }

        public ICommand PostLoginCommand { get; }

        public ICommand GoToRegistrationCommand { get; }

        public bool WarningVisible
        {
            get => _warningVisible;
            set => SetProperty(ref _warningVisible, value);
        }

        public string WarningText
        {
            get => _warningText;
            set => SetProperty(ref _warningText, value);
        }

        public string WarningColor
        {
            get => _warningColor;
            set => SetProperty(ref _warningColor, value);
        }

        public ConnexionViewModel()
        {
            WarningVisible = false;
            PostLoginCommand = new Command(PostLogin);
            GoToRegistrationCommand = new Command(GoToRegistration);
        }

        public async void PostLogin()
        {
            WarningText = "Connexion en cours...";
            WarningColor = "Blue";
            WarningVisible = true;
            LoginRequest loginRequest = new LoginRequest();
            loginRequest.Email = Email;
            loginRequest.Password = Password;

            ApiClient apiClient = new ApiClient();

            HttpResponseMessage response = await apiClient.Execute(HttpMethod.Post, "https://td-api.julienmialon.com/auth/login", loginRequest);
            Response<LoginResult> result = await apiClient.ReadFromResponse<Response<LoginResult>>(response);
            
            if (response.IsSuccessStatusCode)
            {
                App._token = result.Data.AccessToken;
                await App.NavigationService.NavigateAsync("ListeLieuxPage");
                WarningVisible = false;
            }
            else if (result.ErrorMessage == "Invalid credentials")
            {
                WarningText = "Email ou mot de passe invalide";
                WarningColor = "Red";
                WarningVisible = true;
            }
            else
            {
                WarningText = "Connexion impossible";
                WarningColor = "Red";
                WarningVisible = true;
            }
        }

        public async void GoToRegistration()
        {
            await App.NavigationService.NavigateAsync("RegistrationPage");
        }
    }
}
