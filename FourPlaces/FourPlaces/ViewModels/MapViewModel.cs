﻿using Storm.Mvvm;
using Storm.Mvvm.Navigation;
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Input;
using TD.Api.Dtos;
using Xamarin.Forms;
using Xamarin.Forms.Maps;


namespace FourPlaces.ViewModels
{
    class MapViewModel : ViewModelBase
    {
        private Map _map;

        public ICommand GoToProfileCommand { get; }
        public Map Map
        {
            get => _map;
            set => SetProperty(ref _map, value);
        }

        public MapViewModel(PlaceItemSummary placeItem)
        {
            GoToProfileCommand = new Command(GoToProfile);
            Map = new Map(new MapSpan(new Position(placeItem.Latitude, placeItem.Longitude), 0.1, 0.1));

            var pin = new Pin()
            {
                Position = new Position(placeItem.Latitude, placeItem.Longitude),
                Label = placeItem.Title
            };
            Map.Pins.Add(pin);
        }

        public async void GoToProfile()
        {
            await App.NavigationService.NavigateAsync("ProfilePage");
        }
    }
}
