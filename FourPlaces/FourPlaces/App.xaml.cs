﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace FourPlaces
{
    public partial class App : Application
    {
        public static string _token { get; set; }
        public App()
        {
            InitializeComponent();

            NavigationService.Configure("ConnexionPage", typeof(Views.ConnexionPage));
            NavigationService.Configure("RegistrationPage", typeof(Views.RegistrationPage));
            NavigationService.Configure("ListeLieuxPage", typeof(Views.ListeLieuxPage));
            NavigationService.Configure("AddPlacePage", typeof(Views.AddPlacePage));
            NavigationService.Configure("DetailLieuPage", typeof(Views.DetailLieuPage));
            NavigationService.Configure("ProfilePage", typeof(Views.ProfilePage));
            NavigationService.Configure("MapPage", typeof(Views.MapPage));
            NavigationService.Configure("CommentsPage", typeof(Views.CommentsPage));

            var mainPage = ((ViewNavigationService)NavigationService).SetRootPage("ConnexionPage");

            MainPage = mainPage;
        }
        public static INavigationService NavigationService { get; } = new ViewNavigationService();

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
